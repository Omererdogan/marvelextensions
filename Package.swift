// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MarvelExtensions",
    platforms: [.iOS(.v10)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "Coordinator",
            targets: ["Coordinator"]),
        .library(
            name: "CoordinatorRx",
            targets: ["CoordinatorRx"]),
        .library(
            name: "Animations",
            targets: ["Animations"]),
        .library(
            name: "Utils",
            targets: ["Utils"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/ReactiveX/RxSwift.git", from: "6.2.0"),
        .package(url: "https://github.com/RxSwiftCommunity/RxAlamofire.git", from: "6.1.2"),
    ],
    targets: [
        // Targets are the basic   building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "Coordinator",
            dependencies: []),
        .target(
            name: "CoordinatorRx",
            dependencies: ["Coordinator", "RxSwift"]),
        .target(
            name: "Animations",
            dependencies: ["Coordinator"]),
        .target(
            name: "Utils",
            dependencies: ["RxAlamofire"]),
    ]
)
