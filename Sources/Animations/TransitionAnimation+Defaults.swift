//
//  TransitionAnimation+Defaults.swift
//  XCoordinator-Example
//
//  Created by Paul Kraft on 28.12.18.
//  Copyright © 2018 QuickBird Studios. All rights reserved.
//

import UIKit
import Coordinator

public let defaultAnimationDuration: TimeInterval = 0.35

public extension CGFloat {
    static let verySmall: CGFloat = 0.0001
}
