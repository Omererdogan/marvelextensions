//
//  Transitions.swift
//  XCoordinator-Example
//
//  Created by Paul Kraft on 17.09.19.
//  Copyright © 2019 QuickBird Studios. All rights reserved.
//

import UIKit
import Coordinator

public extension Transition {

    static func presentFullScreen(_ wellCoordinators: WellCoordinator, animation: Animation? = nil) -> Transition {
        wellCoordinators.viewController?.modalPresentationStyle = .fullScreen
        return .present(wellCoordinators, animation: animation)
    }

    static func dismissAll() -> Transition {
        return Transition(wellCoordinators: [], animationInUse: nil) { rootViewController, options, completion in
            guard let presentedViewController = rootViewController.presentedViewController else {
                completion?()
                return
            }
            presentedViewController.dismiss(animated: options.animated) {
                Transition.dismissAll()
                    .perform(on: rootViewController, with: options, completion: completion)
            }
        }
    }

}
