//
//  File.swift
//  
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Foundation
import UIKit

extension BaseCoordinator {
    public typealias RootViewController = TransitionType.RootViewController
}

open class BaseCoordinator<RouteType: Route, TransitionType: TransitionProtocol>: Coordinator {
    

    // MARK: Stored properties
    private var removeParentChildren: () -> Void = {}
    private var gestureRecognizerTargets = [GestureRecognizerTarget]()
    
    
    public private(set) var children = [WellCoordinator]()

    public private(set) var rootViewController: RootViewController
    
    open var viewController: UIViewController! {
        rootViewController
    }


    public init(rootViewController: RootViewController, initialRoute: RouteType?) {
        self.rootViewController = rootViewController
        initialRoute.map(prepareTransition).map(performTransitionAfterWindowAppeared)
    }


    public init(rootViewController: RootViewController, initialTransition: TransitionType?) {
        self.rootViewController = rootViewController
        initialTransition.map(performTransitionAfterWindowAppeared)
    }

    open func presented(from wellCoordinator: WellCoordinator?) {}

    public func removeChildrenIfNeeded() {
        children.removeAll { $0.canBeRemovedAsChild() }
        removeParentChildren()
    }
    
    public func addChild(_ wellCoordinator: WellCoordinator) {
        children.append(wellCoordinator)
        wellCoordinator.registerParent(self)
    }
    
    public func removeChild(_ wellCoordinator: WellCoordinator) {
        children.removeAll { $0.viewController === wellCoordinator.viewController }
        removeChildrenIfNeeded()
    }


    open func prepareTransition(for route: RouteType) -> TransitionType {
        fatalError("Please override the \(#function) method.")
    }
    
    public func registerParent(_ wellCoordinator: WellCoordinator & AnyObject) {
        let previous = removeParentChildren
        removeParentChildren = { [weak wellCoordinator] in
            previous()
            wellCoordinator?.childTransitionCompleted()
        }
    }
    
    @available(iOS, unavailable, message: "Please specify the rootViewController in the initializer of your coordinator instead.")
    open func generateRootViewController() -> RootViewController {
        .init()
    }

    private func performTransitionAfterWindowAppeared(_ transition: TransitionType) {
        guard !UIApplication.shared.windows.contains(where: { $0.isKeyWindow }) else {
            return performTransition(transition, with: TransitionOptions(animated: false))
        }

        var windowAppearanceObserver: Any?

        windowAppearanceObserver = NotificationCenter.default.addObserver(
            forName: UIWindow.didBecomeKeyNotification, object: nil, queue: .main) { [weak self] _ in
            windowAppearanceObserver.map(NotificationCenter.default.removeObserver)
            windowAppearanceObserver = nil
            DispatchQueue.main.async {
                self?.performTransition(transition, with: TransitionOptions(animated: false))
            }
        }
    }
}

extension WellCoordinator {

    fileprivate func canBeRemovedAsChild() -> Bool {
        guard !(self is UIViewController) else { return true }
        guard let viewController = viewController else { return true }
        return !viewController.isInViewHierarchy
            && viewController.children.allSatisfy { $0.canBeRemovedAsChild() }
    }

}

extension UIViewController {

    fileprivate var isInViewHierarchy: Bool {
        isBeingPresented
            || presentingViewController != nil
            || presentedViewController != nil
            || parent != nil
            || view.window != nil
            || navigationController != nil
            || tabBarController != nil
            || splitViewController != nil
    }

}

extension BaseCoordinator {

    open func registerInteractiveTransition<GestureRecognizer: UIGestureRecognizer>(
        for route: RouteType,
        triggeredBy recognizer: GestureRecognizer,
        handler: @escaping (_ handlerRecognizer: GestureRecognizer, _ transition: () -> TransitionAnimation?) -> Void,
        completion: PresentationHandler? = nil) {

        let animationGenerator = { [weak self] () -> TransitionAnimation? in
            guard let self = self else { return nil }
            let transition = self.prepareTransition(for: route)
            transition.animation?.start()
            self.performTransition(transition, with: TransitionOptions(animated: true), completion: completion)
            return transition.animation
        }

        let target = Target(recognizer: recognizer) { recognizer in
            handler(recognizer, animationGenerator)
        }

        gestureRecognizerTargets.append(target)
    }


    open func registerInteractiveTransition<GestureRecognizer: UIGestureRecognizer>(
        for route: RouteType,
        triggeredBy recognizer: GestureRecognizer,
        progress: @escaping (GestureRecognizer) -> CGFloat,
        shouldFinish: @escaping (GestureRecognizer) -> Bool,
        completion: PresentationHandler? = nil) {

        var animation: TransitionAnimation?
        return registerInteractiveTransition(
            for: route,
            triggeredBy: recognizer,
            handler: { recognizer, transition in
                switch recognizer.state {
                case .possible, .failed:
                    break
                case .began:
                    animation = transition()
                case .changed:
                    animation?.interactionController?.update(progress(recognizer))
                case .cancelled:
                    defer { animation?.cleanup() }
                    animation?.interactionController?.cancel()
                case .ended:
                    defer { animation?.cleanup() }
                    if shouldFinish(recognizer) {
                        animation?.interactionController?.finish()
                    } else {
                        animation?.interactionController?.cancel()
                    }
                @unknown default:
                    break
                }
            },
            completion: completion
        )
    }

    open func unregisterInteractiveTransitions(triggeredBy recognizer: UIGestureRecognizer) {
        gestureRecognizerTargets.removeAll { target in
            guard target.gestureRecognizer === recognizer else { return false }
            recognizer.removeTarget(target, action: nil)
            return true
        }
    }

}
