//
//  Router+Combine.swift
//  MCoordinatorCombine
//

#if canImport(Combine)

import Combine

public struct PublisherExtension<Base> {
    public let base: Base
}

extension Router {
    public var publishers: PublisherExtension<Self> {
        .init(base: self)
    }
}

@available(iOS 13.0, *)
extension PublisherExtension where Base: Router {

    public func trigger(_ route: Base.RouteType,
                        with options: TransitionOptions = .init(animated: true)
        ) -> Future<Void, Never> {
        Future { completion in
            self.base.trigger(route, with: options) {
                completion(.success(()))
            }
        }
    }

}

#endif
