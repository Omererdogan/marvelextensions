//
//  NavigationTransition.swift
//  MCoordinator
//
//

import UIKit

/// NavigationTransition offers transitions that can be used
/// with a `UINavigationController` as rootViewController.
public typealias NavigationTransition = Transition<UINavigationController>

extension Transition where RootViewController: UINavigationController {

    ///
    /// Pushes a wellCoordinator on the rootViewController's navigation stack.
    ///
    /// - Parameters:
    ///     - wellCoordinator: The wellCoordinator to be pushed onto the navigation stack.
    ///     - animation:
    ///         The animation to set for the wellCoordinator. Its presentationAnimation will be used for the
    ///         immediate push-transition, its dismissalAnimation is used for the pop-transition,
    ///         if not otherwise specified. Specify `nil` here to leave animations as they were set for the
    ///         wellCoordinator before. You can use `Animation.default` to reset the previously set animations
    ///         on this wellCoordinator.
    ///
    public static func push(_ wellCoordinator: WellCoordinator, animation: Animation? = nil) -> Transition {
        Transition(wellCoordinators: [wellCoordinator],
                   animationInUse: animation?.presentationAnimation
        ) { rootViewController, options, completion in
            rootViewController.push(wellCoordinator.viewController,
                                    with: options,
                                    animation: animation
            ) {
                wellCoordinator.presented(from: rootViewController)
                completion?()
            }
        }
    }

    ///
    /// Pops the topViewController from the rootViewController's navigation stack.
    ///
    /// - Parameter animation:
    ///     The animation to set for the wellCoordinator. Only its dismissalAnimation is used for the
    ///     pop-transition. Specify `nil` here to leave animations as they were set for the
    ///     wellCoordinator before. You can use `Animation.default` to reset the previously set animations
    ///     on this wellCoordinator.
    ///
    public static func pop(animation: Animation? = nil) -> Transition {
        Transition(wellCoordinators: [],
                   animationInUse: animation?.dismissalAnimation
        ) { rootViewController, options, completion in
            rootViewController.pop(toRoot: false,
                                   with: options,
                                   animation: animation,
                                   completion: completion)
        }
    }

    ///
    /// Pops viewControllers from the rootViewController's navigation stack until the specified
    /// wellCoordinator is reached.
    ///
    /// - Parameters:
    ///     - wellCoordinator:
    ///         The wellCoordinator to pop to. Make sure this wellCoordinator is in the rootViewController's
    ///         navigation stack before performing such a transition.
    ///     - animation:
    ///         The animation to set for the wellCoordinator. Only its dismissalAnimation is used for the
    ///         pop-transition. Specify `nil` here to leave animations as they were set for the
    ///         wellCoordinator before. You can use `Animation.default` to reset the previously set animations
    ///         on this wellCoordinator.
    ///
    public static func pop(to wellCoordinator: WellCoordinator, animation: Animation? = nil) -> Transition {
        Transition(wellCoordinators: [wellCoordinator],
                   animationInUse: animation?.dismissalAnimation
        ) { rootViewController, options, completion in
            rootViewController.pop(to: wellCoordinator.viewController,
                                   options: options,
                                   animation: animation,
                                   completion: completion)
        }
    }

    ///
    /// Pops viewControllers from the rootViewController's navigation stack until only one viewController
    /// is left.
    ///
    /// - Parameter animation:
    ///     The animation to set for the wellCoordinator. Only its dismissalAnimation is used for the
    ///     pop-transition. Specify `nil` here to leave animations as they were set for the
    ///     wellCoordinator before. You can use `Animation.default` to reset the previously set animations
    ///     on this wellCoordinator.
    ///
    public static func popToRoot(animation: Animation? = nil) -> Transition {
        Transition(wellCoordinators: [],
                   animationInUse: animation?.dismissalAnimation
        ) { rootViewController, options, completion in
            rootViewController.pop(toRoot: true,
                                   with: options,
                                   animation: animation,
                                   completion: completion)
        }
    }

    ///
    /// Replaces the navigation stack of the rootViewController with the specified wellCoordinators.
    ///
    /// - Parameters:
    ///     - wellCoordinators: The wellCoordinators to make up the navigation stack after the transition is done.
    ///     - animation:
    ///         The animation to set for the wellCoordinator. Its presentationAnimation will be used for the
    ///         transition animation of the top-most viewController, its dismissalAnimation is used for
    ///         any pop-transition of the whole navigation stack, if not otherwise specified. Specify `nil`
    ///         here to leave animations as they were set for the wellCoordinators before. You can use
    ///         `Animation.default` to reset the previously set animations on all wellCoordinators.
    ///
    public static func set(_ wellCoordinators: [WellCoordinator], animation: Animation? = nil) -> Transition {
        Transition(wellCoordinators: wellCoordinators,
                   animationInUse: animation?.presentationAnimation
        ) { rootViewController, options, completion in
            rootViewController.set(wellCoordinators.map { $0.viewController },
                                   with: options,
                                   animation: animation
            ) {
                wellCoordinators.forEach { $0.presented(from: rootViewController) }
                completion?()
            }
        }
    }
}
