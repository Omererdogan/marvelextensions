//
//  PageViewTransition.swift
//  MCoordinator
//
//

import UIKit

/// PageTransition offers transitions that can be used
/// with a `UIPageViewController` rootViewController.
public typealias PageTransition = Transition<UIPageViewController>

extension Transition where RootViewController: UIPageViewController {

    ///
    /// Sets the current page(s) of the rootViewController. Make sure to set
    /// `UIPageViewController.isDoubleSided` to the appropriate setting before executing this transition.
    ///
    /// - Parameters:
    ///     - first:
    ///         The first page being shown. If second is specified as `nil`, this reflects a single page
    ///         being shown.
    ///     - second:
    ///         The second page being shown. This page is optional, as your rootViewController can be used
    ///         with `isDoubleSided` enabled or not.
    ///     - direction:
    ///         The direction in which the transition should be animated.
    ///
    public static func set(_ first: WellCoordinator, _ second: WellCoordinator? = nil,
                           direction: UIPageViewController.NavigationDirection) -> Transition {
        let wellCoordinators = [first, second].compactMap { $0 }
        return Transition(wellCoordinators: wellCoordinators,
                          animationInUse: nil
        ) { rootViewController, options, completion in
            rootViewController.set(wellCoordinators.map { $0.viewController },
                                   direction: direction,
                                   with: options
            ) {
                wellCoordinators.forEach { $0.presented(from: rootViewController) }
                completion?()
            }
        }
    }

    static func initial(pages: [WellCoordinator]) -> Transition {
        Transition(wellCoordinators: pages, animationInUse: nil) { rootViewController, _, completion in
            CATransaction.begin()
            CATransaction.setCompletionBlock {
                pages.forEach { $0.presented(from: rootViewController) }
                completion?()
            }
            CATransaction.commit()
        }
    }
}
