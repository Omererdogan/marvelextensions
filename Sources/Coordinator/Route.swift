//
//  Route.swift
//  MCoordinator
//
//

import UIKit

///
/// This is the protocol your route types need to conform to.
///
/// - Note:
///     It has no requirements, although the use of enums is encouraged to make your
///     navigation code type safe.
///
public protocol Route {}
