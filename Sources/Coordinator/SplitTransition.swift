//
//  UISplitViewController+Transition.swift
//  MCoordinator
//
//

import UIKit

///
/// SplitTransition offers different transitions common to a `UISplitViewController` rootViewController.
///
public typealias SplitTransition = Transition<UISplitViewController>

extension Transition where RootViewController: UISplitViewController {

    public static func set(_ wellCoordinators: [WellCoordinator]) -> Transition {
        Transition(wellCoordinators: wellCoordinators, animationInUse: nil) { rootViewController, _, completion in
            CATransaction.begin()
            CATransaction.setCompletionBlock {
                wellCoordinators.forEach { $0.presented(from: rootViewController) }
                completion?()
            }
            autoreleasepool {
                rootViewController.viewControllers = wellCoordinators.map { $0.viewController }
            }
            CATransaction.commit()
        }
    }

}
