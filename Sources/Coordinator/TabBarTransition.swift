//
//  TabBarTransition.swift
//  MCoordinator
//
//

import UIKit

/// TabBarTransition offers transitions that can be used
/// with a `UITabBarController` rootViewController.
public typealias TabBarTransition = Transition<UITabBarController>

extension Transition where RootViewController: UITabBarController {

    ///
    /// Transition to set the tabs of the rootViewController with an optional custom animation.
    ///
    /// - Note:
    ///     Only the presentation animation of the Animation object is used.
    ///
    /// - Parameters:
    ///     - wellCoordinators:
    ///         The tabs to be set are defined by the wellCoordinators' viewControllers.
    ///     - animation:
    ///         The animation to be used. If you specify `nil` here, the default animation by UIKit is used.
    ///
    public static func set(_ wellCoordinators: [WellCoordinator], animation: Animation? = nil) -> Transition {
        Transition(wellCoordinators: wellCoordinators,
                   animationInUse: animation?.presentationAnimation
        ) { rootViewController, options, completion in
            rootViewController.set(wellCoordinators.map { $0.viewController },
                                   with: options,
                                   animation: animation,
                                   completion: {
                                    wellCoordinators.forEach { $0.presented(from: rootViewController) }
                                    completion?()
            })
        }
    }

    ///
    /// Transition to select a tab with an optional custom animation.
    ///
    /// - Note:
    ///     Only the presentation animation of the Animation object is used.
    ///
    /// - Parameters:
    ///     - wellCoordinator:
    ///         The tab to be selected is the wellCoordinator's viewController. Make sure that this is one of the
    ///         previously specified tabs of the rootViewController.
    ///     - animation:
    ///         The animation to be used. If you specify `nil` here, the default animation by UIKit is used.
    ///
    public static func select(_ wellCoordinator: WellCoordinator, animation: Animation? = nil) -> Transition {
        Transition(wellCoordinators: [wellCoordinator],
                   animationInUse: animation?.presentationAnimation
        ) { rootViewController, options, completion in
            rootViewController.select(wellCoordinator.viewController,
                                      with: options,
                                      animation: animation,
                                      completion: completion)
        }
    }

    ///
    /// Transition to select a tab with an optional custom animation.
    ///
    /// - Note:
    ///     Only the presentation animation of the Animation object is used.
    ///
    /// - Parameters:
    ///     - index:
    ///         The index of the tab to be selected. Make sure that there is a tab at the specified index.
    ///     - animation:
    ///         The animation to be used. If you specify `nil` here, the default animation by UIKit is used.
    ///
    public static func select(index: Int, animation: Animation? = nil) -> Transition {
        Transition(wellCoordinators: [],
                   animationInUse: animation?.presentationAnimation
        ) { rootViewController, options, completion in
            rootViewController.select(index: index,
                                      with: options,
                                      animation: animation,
                                      completion: completion)
        }
    }

}
