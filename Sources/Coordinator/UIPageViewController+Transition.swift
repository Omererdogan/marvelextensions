//
//  UIPageViewController+Transition.swift
//  MCoordinator
//

import UIKit

extension UIPageViewController {
    func set(_ viewControllers: [UIViewController],
             direction: UIPageViewController.NavigationDirection,
             with options: TransitionOptions,
             completion: PresentationHandler?) {
        setViewControllers(
            viewControllers,
            direction: direction,
            animated: options.animated,
            completion: { _ in completion?() }
        )
    }
}
