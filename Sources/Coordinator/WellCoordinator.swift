//
//  WellCoordinator.swift
//  
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import UIKit


public protocol WellCoordinator {


    var viewController: UIViewController! { get }

    func router<R: Route>(for route: R) -> StrongRouter<R>?


    func presented(from wellCoordinator: WellCoordinator?)
    
    func registerParent(_ wellCoordinator: WellCoordinator & AnyObject)

    func childTransitionCompleted()


    func setRoot(for window: UIWindow)
}

extension WellCoordinator {
    
    public func registerParent(_ wellCoordinator: WellCoordinator & AnyObject) {}

    public func childTransitionCompleted() {}

    public func setRoot(for window: UIWindow) {
        window.rootViewController = viewController
        window.makeKeyAndVisible()
        presented(from: window)
    }

    public func router<R: Route>(for route: R) -> StrongRouter<R>? {
        self as? StrongRouter<R>
    }

    public func presented(from wellCoordinator: WellCoordinator?) {}
}

extension UIViewController: WellCoordinator {}
extension UIWindow: WellCoordinator {}
